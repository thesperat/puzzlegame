package mortal.toolset;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {

    public void writeToFile(String newTime, int option) throws IOException {
        String stringOption = integerToString(option);
        String path = CONFIG.FilePath + CONFIG.FileName + stringOption + CONFIG.FileFormat;
        List<String> stringsInFile = loadFile(option);
        PrintWriter out = new PrintWriter(path);

        System.out.println("Loaded file:" + stringsInFile.size());

        if(stringsInFile.size() == 0){
            stringsInFile.add(newTime);
        }
        else if(stringsInFile.size() == 4){
            stringsInFile.add(0, newTime);
            stringsInFile.remove(stringsInFile.size()-1);
        }else{
            stringsInFile.add(0, newTime);
        }

        for(String str: stringsInFile){
            out.println(str);
        }
        out.close();



    }
    public List<String> loadFile(int option) throws IOException {
        checkIfFileExists(option);
        String stringOption = integerToString(option);
        String path = CONFIG.FilePath + CONFIG.FileName + stringOption + CONFIG.FileFormat;
        System.out.println("Loading: " + path);
        BufferedReader breader = new BufferedReader(new FileReader(new File(path)));
        List<String> returningList = new ArrayList<>();
        String info;
        while ((info = breader.readLine()) != null){
            System.out.println(info);
            returningList.add(info);
        }
        breader.close();

        System.out.println("Returned size: " + returningList.size());
        return returningList;
    }

    private void checkIfFileExists(int option) throws IOException {
        String path = CONFIG.FilePath + CONFIG.FileName + integerToString(option) + CONFIG.FileFormat;
        File yourFile = new File(path);
        yourFile.createNewFile();

    }
    private String integerToString(int integer){
        return Integer.toString(integer);
    }
}
