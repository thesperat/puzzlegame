package mortal.toolset;

import javafx.scene.image.Image;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImageSpawner{
    public Image image;
    Point pos;

    ImageSpawner() throws FileNotFoundException{
        generateImage();
        generatePosition();

    }
    private void generateImage() throws FileNotFoundException {
        int count = new File(CONFIG.ImageSpawnLocation).list().length;
        String option = Integer.toString((int)(Math.random() * count));

        this.image =  new Image(new FileInputStream(CONFIG.ImageSpawnLocation +
                CONFIG.ImageSpawnName +
                option +
                CONFIG.ImageSpawnFormat));



    }
    private void generatePosition(){
        int width = (int)(Math.random() * 1000) + 100;
        int height = (int)(Math.random() * 100) + 100;

        this.pos =  new Point(width, height);
    }
}
