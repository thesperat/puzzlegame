package mortal.toolset;

import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LocationsGenerator {
    public static Image createLocation() throws FileNotFoundException {

        int count = new File(CONFIG.LocationImagesPath).list().length;
        String locationNumber = Integer.toString((int)(Math.random() * count));

        return new Image(new FileInputStream(CONFIG.LocationImagesPath +
                CONFIG.LocationImageName +
                locationNumber +
                CONFIG.LocationImageFormat));

    }

}
