package mortal.toolset;

public class CONFIG {
    private static String GlobalPath = "C:\\Users\\mmagda\\Documents\\gui2\\src\\mortal\\";

    public static String path = GlobalPath + "images\\puzzles\\";
    public static String imageBaseName = "img";
    public static String imageFormat = ".jpg";

    public static String DialogPanePath = "file:///C:/Users/mmagda/Documents/gui2/src/mortal/FXMLFiles/dialog.css";

    public static String FXMLpath = GlobalPath + "FXMLFiles\\";
    public static String FXMLname = "FXMLGameOption";
    public static String FXMLformat = ".fxml";

    static String FilePath = GlobalPath + "times\\";
    static String FileName = "time";
    static String FileFormat= ".txt";

    static String ClickSoundPath = GlobalPath + "sounds\\misc\\toasty.wav";
    static String WinSoundPath = GlobalPath + "sounds\\misc\\victory.wav";
    static String ThemeSoundPath = GlobalPath + "sounds\\misc\\theme.wav";
    static String LaughSoundPath = GlobalPath + "sounds\\misc\\laugh.wav";
    static String ExitSoundPath = GlobalPath + "sounds\\misc\\exit.wav";

    static String CharacterSoundPath = GlobalPath + "sounds\\";
    static String ChoicePath = "choice.wav";
    static String StartPath = "start.wav";
    static String SubSoundPath = "sub\\";
    static String RaidenSoundPath = "raiden\\";
    static String ScorpionSoundPath = "scorpion\\";

    static String DifficultySoundPath = GlobalPath + "sounds\\difficulty\\";
    static String DifficultyName = "difficulty";
    static String DifficultyFormat = ".wav";

    static String LocationImagesPath = GlobalPath + "images\\locations\\";
    static String LocationImageName = "location";
    static String LocationImageFormat = ".jpg";

    static String ImageSpawnLocation = GlobalPath + "images\\spawners\\";
    static String ImageSpawnName = "spawner";
    static String ImageSpawnFormat = ".gif";


    static int EasterEggDifficulty = 40000; // <0, 49999>

    public static int playAreaDim = 400;
}
