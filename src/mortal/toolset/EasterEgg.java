package mortal.toolset;

import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.io.FileNotFoundException;

public class EasterEgg {
    private ImageView spawnerImage;
    private Timeline timeline;


    public void startSpawning(AnchorPane gamePane){
        if(Tools.easterEggLaunch()){
            randomSpawner(gamePane);
            this.timeline.setCycleCount(3);
            this.timeline.play();
        }
    }
    private void randomSpawner(AnchorPane gamePane){
        this.timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                try {
                    spawnerImage = generateRandom(gamePane);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }), new KeyFrame(Duration.seconds(4), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                gamePane.getChildren().remove(spawnerImage);
            }
        }));
    }

    private ImageView generateRandom(AnchorPane pane) throws  FileNotFoundException{

        ImageSpawner imageSpawner = new ImageSpawner();
        ImageView spawnerImage = new ImageView(imageSpawner.image);
        spawnerImage.setX(imageSpawner.pos.getX());
        spawnerImage.setY(imageSpawner.pos.getY());

        FadeTransition ft = new FadeTransition(Duration.millis(1000), spawnerImage);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setCycleCount(Timeline.INDEFINITE);
        ft.setAutoReverse(true);
        ft.play();

        pane.getChildren().add(spawnerImage);

        return spawnerImage;

    }
}
