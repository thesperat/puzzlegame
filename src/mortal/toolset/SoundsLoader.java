package mortal.toolset;

import javafx.scene.media.AudioClip;

import java.io.File;

public class SoundsLoader {
    private AudioClip clickSoundPlayer;
    private AudioClip victorySoundPlayer;
    private AudioClip themeSoundPlayer;
    private AudioClip laughSoundPlayer;
    private AudioClip exitSoundPlayer;


    public SoundsLoader(){
        this.clickSoundPlayer = new AudioClip(new File(CONFIG.ClickSoundPath).toURI().toString());
        this.victorySoundPlayer = new AudioClip(new File(CONFIG.WinSoundPath).toURI().toString());
        this.themeSoundPlayer = new AudioClip(new File(CONFIG.ThemeSoundPath).toURI().toString());
        this.themeSoundPlayer.setCycleCount(AudioClip.INDEFINITE);
        this.laughSoundPlayer = new AudioClip(new File(CONFIG.LaughSoundPath).toURI().toString());
        this.exitSoundPlayer = new AudioClip(new File(CONFIG.ExitSoundPath).toURI().toString());

    }

    public AudioClip getDifficultySound(int choice){
        String formattedChoice = Integer.toString(choice);
        return new AudioClip(new File(CONFIG.DifficultySoundPath + CONFIG.DifficultyName + formattedChoice + CONFIG.DifficultyFormat).toURI().toString());
    }

    public AudioClip[] getCharSounds(int choice){
        AudioClip choiceSound;
        AudioClip startSound;
        switch (choice){
            case 2:
                choiceSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.SubSoundPath + CONFIG.ChoicePath).toURI().toString());
                startSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.SubSoundPath + CONFIG.StartPath).toURI().toString());
                break;

            case 3:
                choiceSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.RaidenSoundPath + CONFIG.ChoicePath).toURI().toString());
                startSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.RaidenSoundPath + CONFIG.StartPath).toURI().toString());
                break;
            default:
                choiceSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.ScorpionSoundPath + CONFIG.ChoicePath).toURI().toString());
                startSound =  new AudioClip(new File(CONFIG.CharacterSoundPath + CONFIG.ScorpionSoundPath + CONFIG.StartPath).toURI().toString());
                break;

        }

        return new AudioClip[]{choiceSound, startSound};
    }
    public AudioClip getClickSoundPlayer(){
        return this.clickSoundPlayer;
    }
    public AudioClip getVictorySoundPlayer(){
        return this.victorySoundPlayer;
    }
    public AudioClip getThemeSoundPlayer(){
        return this.themeSoundPlayer;
    }
    public AudioClip getLaughSoundPlayer(){
        return this.laughSoundPlayer;
    }
    public AudioClip getExitSoundPlayer(){
        return this.exitSoundPlayer;
    }




}
