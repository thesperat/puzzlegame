package mortal.toolset;

public class Tools {

    public static int changer(String value){
        return Integer.parseInt(value.substring(value.length()-1));
    }

    public static boolean easterEggLaunch(){
        int rand = (int)(Math.random() * 50000);
        System.out.println(rand);
        return rand > CONFIG.EasterEggDifficulty;
    }

}
