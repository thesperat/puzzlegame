package mortal;

import javafx.animation.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import mortal.toolset.*;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;


public class Controller{
    @FXML
    private Button returnButton, exitButton;

    @FXML
    AnchorPane gamePane;

    @FXML
    ImageView gameImageWindow;

    private EasterEgg easterEgg = new EasterEgg();

    @FXML
    private GridPane actualGrid2, resultGrid2, time, timesGrid;

    @FXML
    private ToggleGroup toggleDifficulty, toggleImage;


    private AnimationTimer gameTimer = createTimer();

    //File operator
    private FileOperations fileOp = new FileOperations();
    private int difficulty, image;

    //Puzzle arrays storage
    private PuzzleArrayOperations puzzleArray;
    private PuzzleImage[][] puzzleImageArray;
    private PuzzleImage[][] puzzleImageArrayResult;

    //Loads Sounds
    private SoundsLoader loader = new SoundsLoader();
    private AudioClip clickSound = loader.getClickSoundPlayer();
    private AudioClip winSound = loader.getVictorySoundPlayer();
    private AudioClip laughSound = loader.getLaughSoundPlayer();
    private AudioClip exitSound = loader.getExitSoundPlayer();
    private AudioClip difficultySound;
    private AudioClip[] characterSounds;


    @FXML
    /*
    * Handles main window operations
    * */
    protected void handleMainWindowOptions(ActionEvent event){

        // if difficulty option not selected.
        if (toggleDifficulty.getSelectedToggle() != null) {
            this.difficulty = Tools.changer(((ToggleButton)toggleDifficulty.getSelectedToggle()).getId());
            ((ToggleButton) toggleDifficulty.getSelectedToggle()).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    difficultySound = loader.getDifficultySound(difficulty);
                    difficultySound.play();
                }
            });

        }
        //if image option not selected
        if (toggleImage.getSelectedToggle() != null){
            this.image = Tools.changer(((ToggleButton)toggleImage.getSelectedToggle()).getId());
            ((ToggleButton) toggleImage.getSelectedToggle()).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    characterSounds = loader.getCharSounds(image);
                    characterSounds[0].play();
                }
            });
        }
    }

    @FXML
    /*
     * Handles play button operations
     * */

    protected void playHandle(ActionEvent event ) throws FileNotFoundException {
        if (toggleImage.getSelectedToggle() == null || toggleDifficulty.getSelectedToggle() == null) {
            laughSound.play();

            //generate and show alert window
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Feel the pain...");
            alert.setHeaderText(" ");
            DialogPane x = alert.getDialogPane();
            x.getScene().getStylesheets().add(CONFIG.DialogPanePath);
            x.getStyleClass().remove("alert");
            alert.showAndWait();

        }
        else {


            this.characterSounds[1].play();

            Parent newViewParent = parentCreator(getFXMLGameOption());

            // initialize fields from fxml files
            assert newViewParent != null;
            gamePane = (AnchorPane) newViewParent.lookup("#gamePane");
            resultGrid2 = (GridPane) newViewParent.lookup("#resultGrid2");
            actualGrid2 = (GridPane) newViewParent.lookup("#actualGrid2");
            timesGrid = (GridPane) newViewParent.lookup("#timesGrid");
            time = (GridPane) newViewParent.lookup("#times");
            gameImageWindow = (ImageView) newViewParent.lookup("#gameImageWindow");

            try {
                gameImageWindow.setImage(LocationsGenerator.createLocation());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            try {
                this.puzzleArray = new PuzzleArrayOperations(difficulty, image);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            this.puzzleImageArray = this.puzzleArray.getActualBoard();
            this.puzzleImageArrayResult = this.puzzleArray.getCorrectBoard();

            puzzleDrawBoard(this.puzzleImageArray, actualGrid2);
            puzzleDrawBoardResult(this.puzzleImageArrayResult, resultGrid2);

            for(int x = 0 ; x < this.puzzleArray.getPuzzleBoardDim(); x++){
                for(int y = 0 ; y < this.puzzleArray.getPuzzleBoardDim(); y++){
                    if(this.puzzleImageArray[x][y] != null) {
                        ImageView puzzleImage = this.puzzleImageArray[x][y].getImage();
                        puzzleImage.setOnMouseClicked(new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent mouseEvent) {
                                clickSound.play();
                                puzzleArray.printArray();
                                System.out.println(mouseEvent.getSource());
                                Point pos = findPuzzlePosition(mouseEvent);
                                puzzleArray.movePuzzleToEmptyField(pos);
                                puzzleDrawBoard(puzzleImageArray, actualGrid2);
                                if(puzzleArray.compareArrays()){
                                    winSound.play();
                                    System.out.println("Solved");
                                    gameTimer.stop();

                                    generateTimeList(time.getChildren().toString().substring(18, 27));

                                    puzzleDrawBoardResult(puzzleImageArray, actualGrid2);
                                }
                            }
                        });
                    }
                }
            }

            easterEgg.startSpawning(gamePane);

            Stage stage = (Stage) (((Node) event.getSource()).getScene().getWindow());
            Scene scene = new Scene(newViewParent);
            stage.setScene(scene);
            stage.show();
            // starts game timer
            gameTimer.start();

        }
    }


    @FXML

    /*
     * Handles back to menu operations
     * */
    protected void handleReturn(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("FXMLFiles/FXMLMainFrame.fxml"));
        Stage stage = (Stage) exitButton.getScene().getWindow();

        // if exit button was clicked
        if(exitButton.isFocused()){
            this.exitSound.play();
            while(this.exitSound.isPlaying()){};
            stage.close();
        }

        //if back to menu button was clicked
        if(returnButton.isFocused()){
            //close timer
//            toggleDifficulty = new ToggleGroup();
//            toggleImage = new ToggleGroup();
            System.out.println(event.getSource());
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }
    }

    //Generates result times list
    private void generateTimeList(String newTime){
        try {
            fileOp.writeToFile(newTime, this.difficulty);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<String> str = fileOp.loadFile(this.difficulty);
            timesGrid.getChildren().clear();
            timesGrid.setVisible(true);

            timesGrid.getChildren().add(new Text("Last results"));
            int indexList = 0;
            for(int row = 1; row < str.size()+1; row++){
                if (row == 1) {
                    timesGrid.add(new Text("Your time: " + str.get(indexList)), 0, row);
                }
                else {
                    timesGrid.add(new Text(str.get(indexList)), 0, row);
                }
                indexList++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Creates timer
    private AnimationTimer createTimer(){
        return new AnimationTimer() {
            long startTime;
            double timeInfo = 0;

            @Override
            public void start() {
                super.start();
                startTime = System.currentTimeMillis();
            }

            @Override
            public void stop() {
                super.stop();
            }

            @Override
            public void handle(long now) {
                DateFormat df = new SimpleDateFormat("mm:ss.SSS");
                long newTime = System.currentTimeMillis();
                timeInfo = newTime - startTime;
                time.getChildren().clear();
                Text formatTimer = new Text("Time: " + df.format(timeInfo));
                time.getChildren().add(formatTimer);
            }

        };
    }

    // returns path to FXML file
    private String getFXMLGameOption(){
        return String.format(CONFIG.FXMLpath + CONFIG.FXMLname + "%d" + CONFIG.FXMLformat, difficulty + 1);
    }

    // creates parent
    private Parent parentCreator (String path){
        Parent newViewParent = null;
        try {
            FXMLLoader loader = new FXMLLoader(new File(String.format(CONFIG.FXMLpath + CONFIG.FXMLname + "%d" + CONFIG.FXMLformat, difficulty + 1)).toURI().toURL());
            newViewParent = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newViewParent;
    }

    // finds clicked puzzle position
    private Point findPuzzlePosition( MouseEvent e){
        for(int x = 0; x < this.puzzleImageArray.length; x++){
            for(int y = 0; y < this.puzzleImageArray.length; y++){
                if((this.puzzleImageArray[x][y] != null) && this.puzzleImageArray[x][y].getImage() == e.getSource()){
                    return new Point(x, y);
                }
            }
        }
        return null;
    }

    // draws game puzzle board on Scene

    private void puzzleDrawBoard(PuzzleImage[][] imagesArray, GridPane grid){
        grid.getChildren().clear();


        for(int x = 0 ; x < this.puzzleArray.getPuzzleBoardDim(); x++){
            for(int y = 0 ; y < this.puzzleArray.getPuzzleBoardDim(); y++){
                if(imagesArray[x][y] != null) {
                    ImageView image = imagesArray[x][y].getImage();
                    grid.add(image, x, y);
                }
            }
        }
    }

    // draws result board on Scene
    private void puzzleDrawBoardResult(PuzzleImage[][] imagesArray, GridPane grid){
        grid.getChildren().clear();


        for(int x = 0 ; x < this.puzzleArray.getPuzzleBoardDim(); x++) {
            for (int y = 0; y < this.puzzleArray.getPuzzleBoardDim(); y++) {
                if (imagesArray[x][y] != null) {
                    grid.add(imagesArray[x][y].getImageClone(), x, y);
                }
            }
        }
    }
}
