package mortal;


import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import mortal.toolset.CONFIG;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;


class PuzzleArrayOperations {
    private int difficulty, image, puzzleDim, puzzleCount, puzzleBoardDim;
    private PuzzleImage[][] correctBoard;
    private PuzzleImage[][] actualBoard;

    PuzzleArrayOperations(int difficulty, int image) throws FileNotFoundException{
        this.difficulty = difficulty;
        this.image = image;
        generate();
        System.out.println(compareArrays());

    }

    // creates two puzzle boards
    private void generate() throws FileNotFoundException {
        generateValues();
        this.correctBoard = new PuzzleImage[this.puzzleBoardDim][this.puzzleBoardDim];
        this.actualBoard = new PuzzleImage[this.puzzleBoardDim][this.puzzleBoardDim];

        createPuzzleBoard();
        shufflePuzzleBoard();
        printArray();

    }

    // cuts image into pieces and allocates those object into  array
    private void createPuzzleBoard() throws FileNotFoundException{
        Rectangle2D puzzle;


        int posOnImageY = 0;
        int shift = this.puzzleDim * this.puzzleBoardDim;
        System.out.println(this.puzzleDim);
        for(int y = 0; y < this.puzzleBoardDim; y++){
            int posOnImageX = 0;
            for(int x = 0; x < this.puzzleBoardDim; x++){
                ImageView imageView = new ImageView(loadImage());
                System.out.printf("Getting image from %d %d\n",posOnImageX, posOnImageY);
                puzzle = new Rectangle2D(posOnImageX, posOnImageY, shift , shift);
                imageView.setViewport(puzzle);
                PuzzleImage generatedPuzzle = new PuzzleImage(imageView, puzzle);
                this.correctBoard[x][y] = generatedPuzzle;
                this.actualBoard[x][y] = generatedPuzzle;
                posOnImageX += shift;
            }
            posOnImageY +=  shift;
        }
        this.correctBoard[this.puzzleBoardDim - 1][this.puzzleBoardDim - 1] = null;
        this.actualBoard[this.puzzleBoardDim - 1][this.puzzleBoardDim - 1] = null;


    }


    // shuffle puzzle board
    private void shufflePuzzleBoard(){
        Point emptyField = new Point(this.puzzleBoardDim -1, this.puzzleBoardDim - 1);
        int eX = (int)emptyField.getX();
        int eY = (int)emptyField.getY();
        for(int i = 0; i < 100 + 100 * this.difficulty; i++){
//            System.out.println("shuffle");
            Point move =  puzzleMoveOptions((int)(Math.random()*4));

            if(
                    (eX + move.getX()) >= 0 &&
                    (eX + move.getX() < this.puzzleBoardDim) &&
                    (eY + move.getY()) >= 0  &&
                    (eY + move.getY()) < this.puzzleBoardDim ){
//                System.out.printf("moving empty from %d %d to %f %f\n", eX, eY, move.getX(), move.getY());

                this.actualBoard[eX][eY] = this.actualBoard[(int)(eX + move.getX())][(int)(eY + move.getY())];
                this.actualBoard[(int)(eX + move.getX())][(int)(eY + move.getY())] = null;
                eX += (int)move.getX();
                eY += (int)move.getY();
            }
        }
        if(compareArrays()) shufflePuzzleBoard();
    }

    //moves puzzle to empty filed (null field) switching sides
    void movePuzzleToEmptyField(Point actualPosition){
        System.out.printf("Position in method: (%f, %f)\n", actualPosition.getX(), actualPosition.getY());
        for(int i = 0; i < 4; i++){
            Point option = puzzleMoveOptions(i);

            if(
                    (actualPosition.getX() + option.getX()) >= 0 &&
                    (actualPosition.getX() + option.getX()) < this.puzzleBoardDim &&
                    (actualPosition.getY() + option.getY()) >= 0 &&
                    (actualPosition.getY() + option.getY()) < this.puzzleBoardDim){
                if(this.actualBoard[(int)(actualPosition.getX() + option.getX())][(int)(actualPosition.getY() + option.getY())] == null){
                    this.actualBoard[(int)(actualPosition.getX() + option.getX())][(int)(actualPosition.getY() + option.getY())] = this.actualBoard[(int)actualPosition.getX()][(int)actualPosition.getY()];
                    this.actualBoard[(int)actualPosition.getX()][(int)actualPosition.getY()] = null;
                }
            }
        }
    }

    void printArray(){
        System.out.println(Arrays.deepToString(this.actualBoard));
        System.out.println(Arrays.deepToString(this.correctBoard));

    }

    //compare arrays and return bool value
    boolean compareArrays(){
        return Arrays.deepEquals(this.actualBoard, this.correctBoard);
    }

    private Point puzzleMoveOptions(int option){
        switch (option){
            case 0:
                return new Point(1, 0);
            case 1:
                return new Point(-1, 0);
            case 2:
                return new Point(0, 1);
            case 3:
                return new Point(0, -1);
            default:
                return new Point(1, 0);
        }
    }

    private void generateValues(){
        switch (this.difficulty){
            case 0:
                this.puzzleCount = 4;
                break;
            case 1:
                this.puzzleCount = 9;
                break;
            case 2:
                this.puzzleCount = 16;
                break;
        }
        this.puzzleDim = CONFIG.playAreaDim / this.puzzleCount;
        this.puzzleBoardDim = (int)Math.sqrt(this.puzzleCount);
    }

    //choose image from folder
    private String chooseImage(){
        return String.format(CONFIG.path + CONFIG.imageBaseName + "%d" + CONFIG.imageFormat, this.image);
    }

    // load image
    private Image loadImage() throws FileNotFoundException{
        return new Image(new FileInputStream(chooseImage()));

    }

    PuzzleImage[][] getActualBoard(){
        return this.actualBoard;
    }
    PuzzleImage[][] getCorrectBoard(){
        return this.correctBoard;
    }
    int getPuzzleBoardDim(){
        return this.puzzleBoardDim;
    }

}
