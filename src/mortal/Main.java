package mortal;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import mortal.toolset.SoundsLoader;

public class Main extends Application {


    @Override
    public void start(Stage stage) throws Exception{
        SoundsLoader loader = new SoundsLoader();

        loader.getThemeSoundPlayer().play();

        Parent root = FXMLLoader.load(getClass().getResource("FXMLFiles/FXMLMainFrame.fxml"));

        Scene scene = new Scene(root);

        stage.initStyle(StageStyle.UTILITY);
        stage.setTitle("Mortal puzzle");
        stage.setScene(scene);
        stage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }
}
