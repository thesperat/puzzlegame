package mortal;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;

public class PuzzleImage {
    private ImageView image;
    private Rectangle2D rect;

    PuzzleImage(ImageView image, Rectangle2D rect){
        this.rect = rect;
        this.image = image;

    }

    public ImageView getImage(){
        return this.image;
    }

    ImageView getImageClone(){
        ImageView clone = new ImageView(this.image.getImage());
        clone.setViewport(rect);
        return clone;

    }

}
